import React from 'react';
import style from './recipe.module.css'

const Recipe = ({title, image, calories, ingredients}) => {
    return (
        <div className={style.recipe}>
            <h1>{title}</h1>
            <ul>
                {ingredients.map(item => (
                    <li>{item.text}</li>
                ))}
            </ul>
            <p className={style.calories}>Calories: {Math.round(calories)} </p>
            <img className={style.image} src={image} alt=""/>
        </div>
    );
};

export default Recipe;